import React from 'react'
import { View } from '@tarojs/components'
import { Button } from "@nutui/nutui-react-taro"
import './index.css'

import Taro from '@tarojs/taro';
// 引入 VChart 核心模块
import { VChart } from '@visactor/vchart/esm/core';
// 引入柱状图
import { registerBarChart } from '@visactor/vchart/esm/chart';
// 引入坐标轴、Tooltip、CrossHair组件
import { registerTooltip, registerCartesianCrossHair } from '@visactor/vchart/esm/component';
// 引入 Dom tooltip 逻辑
import { registerDomTooltipHandler } from '@visactor/vchart/esm/plugin';
// 引入微信小程序环境代码
import { registerWXEnv } from '@visactor/vchart/esm/env';
import { VChartSimple } from '@visactor/taro-vchart';

// 注册图表和组件
VChart.useRegisters([
  registerBarChart,
  registerTooltip,
  registerCartesianCrossHair,
  registerDomTooltipHandler,
  registerWXEnv
]);

function Index() {

  const spec = {
    type: 'bar',
    data: [
      {
        id: 'barData',
        values: [
          { month: 'Monday', sales: 22 },
          { month: 'Tuesday', sales: 13 },
          { month: 'Wednesday', sales: 25 },
          { month: 'Thursday', sales: 29 },
          { month: 'Friday', sales: 38 }
        ]
      }
    ],
    xField: 'month',
    yField: 'sales'
  };
  
  return (
    <View className="nutui-react-demo">
      <View className="index">
        欢迎使用 NutUI React 开发 Taro 多端项目。
      </View>
      <View className="index">
        <Button type="primary" className="btn">
          NutUI React Button
        </Button>
      </View>
      <VChartSimple
        type={Taro.getEnv() as any}
        style={{ width: '100%', height: '160px' }}
        spec={spec}
        canvasId="chart-id"
      />
    </View>
  )
}

export default Index
